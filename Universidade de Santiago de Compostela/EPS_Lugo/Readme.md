# Acceso aos modelos proporcionados polo centro

A Escola Politécnica Superior de Enxeñaría da Universidade de Santiago de Compostela proporciona modelos de TFG e TFM en formato libre (ODF e LaTeX), aos que se pode acceder dende a [páxina web do centro](https://www.usc.gal/gl/centro/escola-politecnica-superior-enxenaria/tfg).




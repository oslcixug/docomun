# Acceso aos modelos proporcionados polo centro

A Escola Superior de Enxeñaría Informática da Universidade de Vigo proporciona modelos de TFG e TFM en formato libre (ODF e LaTeX), aos que se pode acceder dende a páxina web do centro:

* [TFG](https://esei.uvigo.es/docencia/traballos-fin-de-carreira/traballo-fin-de-grao/).
* [TFM](https://esei.uvigo.es/docencia/traballos-fin-de-carreira/traballo-fin-de-master/).

Tamén se poden consultar as instrucións para a elaboración e estrutura da documentación do TFG na guía presente neste mesmo directorio.

# Proxecto DOCOMUN

## O proxecto DOCOMUN é unha recompilación de modelos baseados en formatos abertos para a elaboración de traballos universitarios, como poden ser os Traballos de Fin de Grao ou de Mestrado.

Dentro deste repositorio atópanse modelos de documentos para realizar diferentes traballos universitarios e, nomeadamente, os modelos para realizar o Traballo de Fin Grao (TFG) e Traballo de Fin de Mestrado (TFM) de numerosos centros das tres universidades públicas galegas.

Todos os modelos de documentos dispoñibles neste repositorio teñen sido elaborados «ex novo» polo persoal da Oficina se Software Libre do CIXUG a partires da normativa aprobada e publicada polas respectivas universidade e/ou centros para a definición dos requisitos formais que deben cumprir os diferentes traballos. Están categorizados por universidade e por centro, e poden descargarse de xeito libre grazas á súa licenza Creative Commons BY-SA.

Se ves algún erro ou non atopas o modelo do teu centro, escríbenos a osl@cixug.es para ver a posibilidade de desenvolver e incluír o modelo neste repositorio.

### Táboa de substitución de tipos de letra

Os tipos de letra empregados nos modelos publicados neste repositorio foron substituídos segundo a seguinte táboa de equivalencias dende un tipo de letra privativo a outro con características tipográficas similares e cunha licenza libre:

Tipo de letra orixinal | Substituída por | Licenza
--- | --- | ---  
Calibri | Carlito | [Licenza][carlito]
Arial | Arimo | [Licenza][arimo]
Times New Roman | Tinos | [Licenza][tinos]
Cambria | Caladea | [Licenza][caladea]
Courier New | Cousine | [Licenza][cousine]
Georgia | Neuton | [Licenza][neuton]

Esta iniciativa enmárcase no Plan de Acción de Software Libre 2023 da Xunta de Galicia, dentro das liñas de actuación para mellorar e potenciar os mecanismos de formación e difusión do software de fontes abertas entre a cidadanía galega e en particular no estudando universitario.

[![CC BY-SA 4.0][cc-by-sa-shield]][cc-by-sa]

Os contidos (incluíndo todos os ficheiros en formato ODT/PDF) deste repositorio están dispoñibles baixo unha licenza
[Creative Commons Attribution-ShareAlike 4.0 International License][cc-by-sa].

[![CC BY-SA 4.0][cc-by-sa-image]][cc-by-sa]

[cc-by-sa]: http://creativecommons.org/licenses/by-sa/4.0/
[cc-by-sa-image]: https://licensebuttons.net/l/by-sa/4.0/88x31.png
[cc-by-sa-shield]: https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg
[carlito]: https://github.com/googlefonts/carlito
[arimo]: https://github.com/googlefonts/Arimo
[tinos]: https://github.com/googlefonts/tinos
[caladea]: https://fonts.google.com/specimen/Caladea
[cousine]: https://fonts.google.com/specimen/Cousine/license
[neuton]: https://online-fonts.com/fonts/neuton
---

![entidades amtega e cixug](imaxes/logo_amtega_cixug.png)

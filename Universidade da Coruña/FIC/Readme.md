# Acceso aos modelos proporcionados polo centro

A Facultade de Informática da Universidade da Coruña proporciona modelos de TFG e TFM en formato libre (ODF e LaTeX), aos que se pode acceder dende a [páxina web do centro](https://fic.udc.es/gl/traballo-fin-de-grao).



